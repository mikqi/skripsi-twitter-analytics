var _ = require('lodash');
var async = require('async');
var step = require('step');
var Xray = require('x-ray');
var token = require('./modules/nalapa').tokenizer;
var clean = require('./modules/nalapa').cleaner;
var Word = require('./modules/nalapa').word;
var feature = require('./modules/nalapa').feature;
var Step = require('step');
var unEscape = require('./modules/unescape');

var x = Xray();

var User = require('../models/User');
var Article = require('../models/Article');
var Category = require('../models/Category');
var WordsCategory = require('../models/WordsCategory');

/**
 * Split into declaration and initialization for better startup performance.
 */
var cheerio;
var Twit;
var request;

console.log(Word.stem('bersekolah'));

/**
 * GET /api
 * List of API examples.
 */
exports.getApi = function (req, res) {
  res.render('api/index', {
    title: 'API Examples'
  });
};

exports.getUser = function (req, res) {
  User.find({}, function (err, data) {
    if (err) {
      res.send(err)
    }

    res.json(data)
  })
}


/**
 *
 * GET /api/wikipedia/:query
 * Scrapping Wikipedia page
 * and return a json artilce wikipedia
 */
exports.getWikipedia = function (req, res, next) {
  var article = req.params.article.replace(/\s|%20/g, '_');

  x('https://id.wikipedia.org/wiki/' + article, '#content', {
    title: '#firstHeading',
    article: '#mw-content-text@html',
    kategori: ['#mw-normal-catlinks ul li']
  })(function (err, data) {
    console.log(data);
    if (data.article.search(/Tidak ada teks di halaman ini/) === -1) {
      res.send(data)
    } else {
      res.json({
        status: 404,
        message: 'Kata kunci yang anda cari tidak ditemukan'
      })
    }
  })
}

/**
 *
 * GET /api/article
 * Get all article from article collections
 */
exports.getArticle = function (req, res, next) {
  Article.find({}, function (err, data) {
    if (err) {
      throw err;
    }

    res.json(data);
  })
}

/**
 *
 * POST /api/article
 * Add new article to article collections
 */
exports.addArticle = function (req, res, next) {
  var subCategory = req.body.subCategory.replace(/\s/g,  '').split(',');

  var data = {
    title: req.body.title,
    article: req.body.article,
    category: {
      categoryName: req.body.categoryName,
      subCategory: subCategory
    },
    preStatus: false
  }

  var newArticle = new Article(data)

  console.log(data)
  newArticle.save(function (response) {
    console.log(response);
    res.send(response)
  })
}

/**
 * POST /api/preproses/
 */
exports.doPreprocessById = function (req, res, next) {
  var id = req.body.id;
  console.log(req.body.id)
  Article.findByIdAndUpdate(id, {'preStatus': true}, function (err, data) {
    console.log(data);
    Step(removeTags, changeUnicode, removeURL, removeSymbol, doCaseFodling, removeWhiteSpace, doTokenizing, doStemming, doCleaning, insertWordsToTable, concatCollections, showResult)

    function removeTags() {
      console.log('Remove Tags');
      return data.article.replace(/<(?:.|\n)*?>/gm, ' ');
    }

    function changeUnicode(err, text) {
      console.log('UNESCAPE');
      return unEscape(text);
    }

    function removeWhiteSpace(err, text) {
      console.log('UNESCAPE');
      return text.replace(/\s\s+|\n/g, ' ');
    }

    function removeURL(err, text) {
      // console.log(text);
      return text.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
    }

    function removeSymbol(err, text) {
      return text.replace(/[^a-zA-Z ]/g, '');
    }

    function doCaseFodling(err, text) {
      return text.toLowerCase();
    }

    function doTokenizing(err, text) {
      return token.tokenize(text)
    }

    function doStemming(err, text) {
      var stemResult = [];

      text.forEach(function (e, i) {
        stemResult.push(Word.stem(e))
      })

      return text;
    }

    function doCleaning(err, text) {
      var nonStopWords = [];
      text.forEach(function (e, i) {
        if (!Word.isStopword(e) && e.length !== 1 && e !== 'xb' && e !=='xbc' && e !=='mm') {
          nonStopWords.push(e);
        }
      })
      return nonStopWords;
    }

    function insertWordsToTable(err, words) {
      var words = {
        title: data.title,
        categoryName: data.category.categoryName,
        words: words
      }

      var saveWords = new WordsCategory(words);

      saveWords.save(function (err) {
        console.log(err)
      })

      return words;
    }

    function concatCollections(err) {
      WordsCategory.find({}, function (err, data) {
        return data;
      });
    }

    function showResult(err, text) {
      res.send({
        message: 'Remove Tags',
        data:text
      })
      console.log(text);
    }

  })
}

/**
 * POST /api/preproses
 */
exports.doPreprocess = function (req, res, next) {

}
/**
 *
 * GET /api/category
 * Get all category from category collections
 */
exports.getCategory = function (req, res, next) {
  Category.find({}, function (err, category) {
    if (err) {
      throw err;
    }

    res.json(category);

  })
}

/**
 *
 * POST /api/category
 * add new category
 */
exports.addCategory = function (req, res, next) {
    var newCategory = new Category({
      categoryName: req.body.category
    });
    if(req.body.category !== '' && req.body.category !== null && req.body.category !== undefined && req.body.category !== 'undefined') {
      newCategory.save(function (err) {
        if (err) {
          console.log(err);
          res.json({
            status: 400,
            message: 'Failed add category ' + req.body.category
          })
        } else {
          res.json({
            status: 200,
            message: 'Success add category ' + req.body.category
          })
        }
      })
    } else {
      res.json({
        status: 400,
        message: 'Categery cant null'
      })
    }
  }

exports.getFileUpload = function (req, res, next) {
  res.render('api/upload', {
    title: 'File Upload'
  });
};

exports.postFileUpload = function (req, res, next) {
  req.flash('success', {
    msg: 'File was uploaded successfully.'
  });
  res.redirect('/api/upload');
};
