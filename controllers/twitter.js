var Twit = require('twit');
var _ = require('lodash');
var CronJob = require('cron').CronJob;

var User = require('../models/User');
var Schedule = require('../models/ScheduleTweet');
/**
 * POST /api/twitter
 * Post a tweet.
 */

// SCHEDULE TWEET CHECK EVERY 1 MUNITES
var job = new CronJob({
  cronTime: '*/5 * * * * *',
  onTick: function () {
    Schedule.find({}, function (err, data) {
      console.log(data);
      var today = new Date();

      data.forEach(function (item) {
        if (today > item.time) {
          console.log('harusnya di tweet plus dihapus');

          var T = new Twit({
            consumer_key: process.env.TWITTER_KEY,
            consumer_secret: process.env.TWITTER_SECRET,
            access_token: item.token[0].accessToken,
            access_token_secret: item.token[0].tokenSecret,
          });

          T.post('statuses/update', {
            status: item.tweet,
          }, function (err, data, response) {
            console.log(item._id + ' Tweeted');
          });

          Schedule.findByIdAndRemove(item._id, function (err, data) {
            console.log(item._id + ' dihapus');
          });
        } else {
          console.log('belum di tweet');
        }

      });
    });
  },

  start: false,
  timeZone: 'Asia/Jakarta',
});

job.start();

exports.postTwitter = function (req, res, next) {

  var token = _.find(req.user.tokens, {
    kind: 'twitter',
  });

  console.log(token);
  console.log(process.env.TWITTER_KEY);
  console.log(process.env.TWITTER_SECRET);

  var T = new Twit({
    consumer_key: process.env.TWITTER_KEY,
    consumer_secret: process.env.TWITTER_SECRET,
    access_token: token.accessToken,
    access_token_secret: token.tokenSecret,
  });
  T.post('statuses/update', {
    status: req.body.tweet,
  }, function (err, data, response) {
    if (err) {
      return next(err);
    }

    if (response.statusCode === 200) {
      res.json({
        statusCode: response.statusCode,
        message: 'Tweet berhasil di post',
        tweet: data.text,
        user: req.user,
      });
    }else {
      res.json({
        statusCode: 500,
        message: 'Cek kembali tweet anda',
      });
    }

  });
};

/**
 * POST /api/twitter/save
 * Save tweet for schedule Tweet
 */
exports.saveTweet = function (req, res, next) {

  console.log(req.body);

  var schedule = {
    token: req.user.tokens,
    tweet: req.body.tweet,
    userId: req.user._id,
    time: new Date(req.body.date),
  };

  console.log(schedule);

  var newSchdule = new Schedule(schedule);

  newSchdule.save(function (err, data) {
    console.log(data);
    res.json({
      data: data,
      statusCode: 200,
      message: 'Tweet berhasil disimpan.',
    });
  });
};

/**
 * GET /api/twitter
 * Twiter API example.
 */
exports.getTwitter = function (req, res, next) {

  var token = _.find(req.user.tokens, {
    kind: 'twitter',
  });
  var T = new Twit({
    consumer_key: process.env.TWITTER_KEY,
    consumer_secret: process.env.TWITTER_SECRET,
    access_token: token.accessToken,
    access_token_secret: token.tokenSecret,
  });

  T.get('application/rate_limit_status', function (err, data) {
    console.log(data);
  });

  // var stream = T.stream('statuses/sample');

  // stream.on('tweet', function (tweet) {
  //   console.log(tweet);
  // });

  // step(readFollower, nextFollower)

  // function readFollower() {
  //   T.get('followers/list', { count: 200  } ,function (err, data) {
  //     if(err) {
  //       return next(err);
  //     }

  //     console.log(data.users.length);
  //     console.log(data.next_cursor)
  //     return data.next_cursor;
  //   })
  //   return 0;
  // }

  // function nextFollower(err, Next) {
  //   T.get('followers/list', {
  //    count: 200, cursor: 1426573622863521500 }, function (err, nextData) {
  //     console.log('masuk next')
  //     if(err) {
  //       return next(err);
  //     }

  //     console.log(nextData.users.length);
  //   })
  // }

  T.get('search/tweets', {
    q: 'nodejs since:2013-01-01',
    geocode: '40.71448,-74.00598,5mi',
    count: 10,
  }, function (err, reply) {
    if (err) {
      return next(err);
    }

    res.render('api/twitter', {
      title: 'Twitter API',
      tweets: reply.statuses,
    });
  });

};
