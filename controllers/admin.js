var _ = require('lodash');
var async = require('async');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var passport = require('passport');
var User = require('../models/User');

/**
 * GET /login
 * Login page.
 */
exports.getLogin = function(req, res) {
  if (req.user) {
    return res.redirect('/admin/dashboard');
  }
  res.render('admin/login', {
    title: 'Login'
  });
};

exports.getDashboard = function (req, res) {
  if(req.users) {
    return res.redirect('/admin');
  }

  if(req.user.status !== 'admin') {
    return res.redirect('/')
  }

  res.render('admin/dashboard', {
    title: 'Dashboard'
  })
  console.log(req.user);
}

/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = function(req, res, next) {
  req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

  var errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.redirect('/admin');
  }

  passport.authenticate('local', function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      req.flash('errors', info);
      return res.redirect('/admin');
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }

      console.log(info);
      req.flash('success', { msg: 'Success! You are logged in.' });
      res.redirect('/admin/dashboard');
    });
  })(req, res, next);
};

/**
 *
 * GET /api/user/active
 * Get data user active
 */
exports.userActive = function (req, res, next) {
  console.log(req.user);
  res.send(req.user);
}
