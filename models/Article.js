var mongoose = require('mongoose');

var articleSchema = new mongoose.Schema({
  title: {
    type:String,
    unique: true
  },
  article: String,
  category: {
    categoryName: String,
    subCategory: [String]
  },
  preStatus: Boolean
}, {
  timestamps: true
});

var Article = mongoose.model('Article', articleSchema);

module.exports = Article;
