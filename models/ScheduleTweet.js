var mongoose = require('mongoose');

var ScheduleTweetsSchema = new mongoose.Schema({
  token: Array,
  tweet: String,
  userId: String,
  time: Date,
}, {
  timestamps: true,
});

var ScheduleTweets = mongoose.model('ScheduleTweets', ScheduleTweetsSchema);

module.exports = ScheduleTweets;
