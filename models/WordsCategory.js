var mongoose = require('mongoose');

var wordsCategorySchema = new mongoose.Schema({
  title: {
    type: String,
    unique: true
  },
  categoryName: String,
  words: [String]
}, {
  timestamps: true
});

var WordsCategory = mongoose.model('WordsCategory', wordsCategorySchema);

module.exports = WordsCategory;
