var mongoose = require('mongoose');

var categorySchema = new mongoose.Schema({
  categoryName: {
    type: String,
    unique: true,
  },
}, {
  timestamps: true
});

var Category = mongoose.model('Category', categorySchema);

module.exports = Category;
