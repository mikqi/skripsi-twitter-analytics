var mongoose = require('mongoose');

var wordsWeightCategorySchema = new mongoose.Schema({
  categoryName: {
    type: String,
    unique: true
  },
  words: [{
    word: String,
    weight: Number
  }]
}, {
  timestamps: true
});

var WordsWeightCategory = mongoose.model('WordsWeightCategory', wordsWeightCategorySchema);

module.exports = WordsWeightCategory;
