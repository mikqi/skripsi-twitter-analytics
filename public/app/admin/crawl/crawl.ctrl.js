angular.module('SkripsiTwitter')
  .controller('CrawlCtrl', CrawlCtrl);

CrawlCtrl.$inject = ['CrawlService', 'KategoriService'];
function CrawlCtrl(CrawlService, KategoriService) {
  var vm = this;

  KategoriService.getAllData().then(function (kategori) {
    console.log(kategori)

    vm.kategori = kategori.data;
  })

  vm.search = function (query) {
    CrawlService.doCrawl(query).then(function (response) {
      console.log(response);
      vm.article = response;
      window.article = response.data;
      $('#article').html(response.data.article)
    });
  }
  var articleData = {}
  vm.save = function () {
    articleData = {
      title: vm.article.data.title,
      article: vm.article.data.article,
      categoryName: vm.categoryName,
      subCategory: vm.article.data.kategori.join(',')
    }
    console.log(articleData);
    CrawlService.saveArticle(articleData);

    console.log(articleData)
  }
}