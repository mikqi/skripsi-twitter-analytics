angular
  .module('SkripsiTwitter')
  .factory('CrawlService', CrawlService);

CrawlService.$inject = ['$http', '$q'];

var urlWiki = location.origin + '/api/wiki';
var urlArticle = location.origin + '/api/article';

function CrawlService($http, $q) {
  return {
    doCrawl: doCrawl,
    saveArticle: saveArticle
  };

  function doCrawl(query) {
    var def = $q.defer();

      return $http.get(urlWiki + '/' + query)
      .success(getDataWiki)
      .error(errorDataWiki);

      function getDataWiki(response) {
        def.resolve(response);
      }

      function errorDataWiki() {
        def.reject('Gagal ambil data Wiki');
      }

      return def.promise;
  }

  function saveArticle(data) {
    $http({
      method: 'POST',
      url: urlArticle,
      data: data
    }).success(postArticle);

    function postArticle(response) {
      console.log(response);
    }
  }
}