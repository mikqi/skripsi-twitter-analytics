angular.module('SkripsiTwitter')
  .controller('KategoriAddCtrl', KategoriAddCtrl)
  .controller('KategoriViewCtrl', KategoriViewCtrl);

KategoriAddCtrl.$inject = ['KategoriService'];

function KategoriAddCtrl(KategoriService) {
  var vm = this;

  vm.today = new Date();

  vm.save = function (category) {
    var data = {
      category: category,
    };
    console.log(data);
    KategoriService.postDataKategori(data);
  };
}

KategoriViewCtrl.$inject = ['KategoriService'];

function KategoriViewCtrl(KategoriService) {
  var vm = this;
  KategoriService.getAllData().then(getKategori);

  function getKategori(data) {
    console.log(data);
    vm.categories = data.data;
  }
}