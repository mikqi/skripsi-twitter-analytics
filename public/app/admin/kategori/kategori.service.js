angular
  .module('SkripsiTwitter')
  .factory('KategoriService', KategoriService);

KategoriService.$inject = ['$http', '$q'];

var urlKategori = location.origin + '/api/category'

function KategoriService($http, $q) {
    return {
      getAllData: getAllData,
      getData: getData,
      postDataKategori: postDataKategori,
      updateKategori: updateKategori,
      deleteKategori: deleteKategori,
    };

    function getAllData() {
      var def = $q.defer();

      return $http.get(urlKategori)
      .success(getDataKategori)
      .error(errorDataKategori);

      function getDataKategori(response) {
        def.resolve(response);
      }

      function errorDataKategori() {
        def.reject('Gagal ambil data kategori');
      }

      return def.promise;

    }

    function getData(id) {
      var def = $q.defer();

      return $http.get(urlKategori + '/' + id)
      .success(getDataKategori)
      .error(errorDataKategori);

      function getDataKategori(response) {
        def.resolve(response);
      }

      function errorDataKategori() {
        def.reject('Gagal ambil data kategori');
      }

      return def.promise;

    }

    function postDataKategori(data) {
      return $http({
                url: urlKategori,
                method: 'POST',
                data: data,
              }).success(postDataKategori);

      function postDataKategori(response) {
        console.log(response);
        return response;
      }
    }

    function updateKategori(data) {
      return $http({
                url: urlKategori,
                method: 'PUT',
                data: data,
              }).success(updateDataKategori);

      function updateDataKategori(response) {
        console.log(response);
        return response;
      }
    }

    function deleteKategori(id) {
      return $http({
        url: urlKategori + '/' + id,
        method: 'DELETE',
      });
    }
  }
