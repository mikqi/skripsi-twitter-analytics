angular.module('SkripsiTwitter', ['ui.router', 'ngSanitize'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('dashboard', {
        url: '/',
        templateUrl: '../../app/admin/dashboard/dashboard.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard'
      })
      .state('crawl', {
        url: '/crawl',
        templateUrl: '../../app/admin/crawl/crawl.html',
        controller: 'CrawlCtrl',
        controllerAs: 'crawl'
      })
      .state('kategori-add', {
        url: '/kategori/add',
        templateUrl: '../../app/admin/kategori/input.html',
        controller: 'KategoriAddCtrl',
        controllerAs: 'kategoriAdd'
      })
      .state('kategori-view', {
        url: '/kategori',
        templateUrl: '../../app/admin/kategori/view.html',
        controller: 'KategoriViewCtrl',
        controllerAs: 'kategoriView'
      })
      .state('prepros-view', {
        url: '/prepros',
        templateUrl: '../../app/admin/prepros/view.html',
        controller: 'PreprosViewCtrl',
        controllerAs: 'preprosView'
      })
      .state('prepros-result', {
        url: '/prepros/result',
        templateUrl: '../../app/admin/prepros/result.html',
        controller: 'PreprosResultCtrl',
        controllerAs: 'preprosResult'
      })
      .state('user', {
        url: '/user',
        templateUrl: '../../app/admin/user/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      })
  })
