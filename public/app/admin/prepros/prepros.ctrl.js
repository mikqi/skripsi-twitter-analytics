angular.module('SkripsiTwitter')
  .controller('PreprosViewCtrl', PreprosViewCtrl)
  .controller('PreprosResultCtrl', PreprosResultCtrl);

PreprosViewCtrl.$inject = ['PreprosService'];
function PreprosViewCtrl(PreprosService) {
  var vm = this;

  PreprosService.getAllArticle().then(getAllArticle);
  function getAllArticle(response) {
    console.log(response);
    vm.articles = response.data;
    console.log(vm.articles);
  }

  vm.preproses = function (id) {

    console.log(id)
    PreprosService.doPreprocessById(id);

  }
}

PreprosResultCtrl.$inject = [];
function PreprosResultCtrl() {
  var vm = this;
}