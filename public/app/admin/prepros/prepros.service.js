angular
  .module('SkripsiTwitter')
  .factory('PreprosService', PreprosService);

PreprosService.$inject = ['$http', '$q'];

var urlArticle = location.origin + '/api/article';
var urlPreprosesById = location.origin + '/api/preproses'

function PreprosService($http, $q) {
  return {
    getAllArticle: getAllArticle,
    doPreprocessById: doPreprocessById
  };

  function getAllArticle() {
    var def = $q.defer();

      return $http.get(urlArticle)
      .success(getDataArticle)
      .error(errorDataArticle);

      function getDataArticle(response) {
        def.resolve(response);
      }

      function errorDataArticle() {
        def.reject('Gagal ambil data Wiki');
      }

      return def.promise;
  }

  function doPreprocessById(id) {
    $http({
      url: urlPreprosesById,
      method: 'POST',
      data: {id:id}
    }).success(function (response) {
      console.log(response);
    })
  }
}