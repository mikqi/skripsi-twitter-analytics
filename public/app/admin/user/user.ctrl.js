angular.module('SkripsiTwitter')
  .controller('UserCtrl', UserCtrl);

UserCtrl.$inject = ['$http'];
function UserCtrl($http) {
  var vm = this;

  vm.today = new Date();
  console.log(location.origin)
  $http.get(location.origin + '/api/user').then(function (user) {
    vm.userList = user.data;
    console.table(user);
    window.user = user;
  })
}