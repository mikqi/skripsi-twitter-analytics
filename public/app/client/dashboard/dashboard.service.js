angular.module('TwikipediaClient')
  .factory('TwitterService', TwitterService);

TwitterService.$inject = ['$q', '$http'];
function TwitterService($q, $http) {
  return {
    postTweet: postTweet,
    saveTweet: saveTweet,
  };

  function postTweet(tweet) {
    return $http({
      method: 'POST',
      url: location.origin + '/api/twitter',
      data: tweet,
    }).success(postTweet);

    function postTweet(response) {
      console.log(response);
      return response;
    }
  }

  function saveTweet(tweet) {
    var tanggal = {
      jam: tweet.time.getHours(),
      menit: tweet.time.getMinutes(),
      tahun: tweet.date.getFullYear(),
      tanggal: tweet.date.getDate(),
      bulan: tweet.date.getMonth(),
    };
    var tanggalnya = new Date(tanggal.tahun, tanggal.bulan, tanggal.tanggal, tanggal.jam, tanggal.menit);
    console.log(tanggal, tanggalnya);

    tweet.date = tanggalnya;
    console.log(tweet);

    return $http({
      method: 'POST',
      url: location.origin + '/api/twitter/save',
      data: tweet,
    }).success(postSaveTweet);

    function postSaveTweet(response) {
      console.log(response);
      return response;
    }

  }
}
