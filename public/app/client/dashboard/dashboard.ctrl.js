angular.module('TwikipediaClient')
  .controller('DashboardCtrl', DashboardCtrl);

DashboardCtrl.$inject = ['$http', 'TwitterService'];
function DashboardCtrl($http, TwitterService) {
  var vm = this;

  vm.postTweet = function (data) {
    console.log(data);
    TwitterService.postTweet(data);
  };

  vm.saveTweet = function (data) {
    console.log(data);
    TwitterService.saveTweet(data);
  };
}
